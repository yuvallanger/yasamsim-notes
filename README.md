# YasamSim development notes

## 2015-07-31

TODO:

Integrate acceleration instead of using impulse velocity
Add dampening

Thanks Welkin!

Also, there's a [screen capture][yasamsim_2015_07_31] of the current progress:

![YasamSim 2015-07-31][yasamsim_2015_07_31]

The yasamnik sprite was chissled out of pure pixels by [moomoohk].

## 2015-07-30

Our hero can now face in the direction it is walking. Hurray!

### 21:29

A few short term goals:

* Convert the png assets into a gloss Picture.
* Figure out what's the deal with FRP and how to use it here.

[yasamsim_2015_07_31]: </yasamsim-2015-07-31.gif>
[moomoohk]: <https://moomoohk.github.io/>
